package cz.ssps.chess.ai;

import android.util.Log;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Random;

import cz.ssps.chess.chess.Board;
import cz.ssps.chess.chess.Move;
import cz.ssps.chess.pieces.Bishop;
import cz.ssps.chess.pieces.Knight;
import cz.ssps.chess.pieces.Pawn;
import cz.ssps.chess.pieces.Piece;
import cz.ssps.chess.pieces.Queen;
import cz.ssps.chess.pieces.Rook;

// https://en.wikipedia.org/wiki/Minimax
public class AI {

    public static int nodes = 0;

    public static final boolean randomIfEqual = true;
    public static final int randomIfEqualChance = 7; // one in .. chance

    public static final int PROMOTION_WEIGHT = 30;
    public static final int CAPTURE_WEIGHT = 10;

    public static Random random = new Random();


    // white - true - maximizing, black - false - minimizing
    // first - is it the first iteration of minimax
    public static AiMove minimax (AiInputs inputs) {
        return minimax(inputs.getBoard(), 0, inputs.getAiDepth(), inputs.whitePlays(),-9999999, 9999999, true );
    }
    public static AiMove minimax(Piece[] board, int depth, int maxDepth, final boolean maximizing, int alpha, int beta, boolean first) {
        switch (Board.checkForWinner(board, maximizing)) {
            case White:
                return new AiMove(9999, depth);
            case Black:
                return new AiMove(-9999, depth);
            case Draw:
                return new AiMove(0, depth);
        }
        // if None, continue playing

        nodes++;

        if (depth == maxDepth) {
            return new AiMove(Board.getScore(board), depth);
        }

//        Log.d("PlayAI", "minimax called depth=" + depth + ", side=" + (maximizing ? "max(white)" : "min(black)"));


        ArrayList<AiMove> possibleMoves = generatePossibleMoves(board, depth, maximizing);


        if (possibleMoves.size() == 1) {
            AiMove move = possibleMoves.get(0);
            move.aiDepth = depth;
            return move;
        }


        // depth=5
        // no sort:
//        I/PlayAI: Minimax searched 27623 nodes, white(max)=false, minimax_score=1
//        Took 7247 ms

        // with sort:
//        I/PlayAI: Minimax searched 11609 nodes, white(max)=false, minimax_score=1
//        Took 3096 ms




        // sort the moves - better moves come first - more alpha-beta pruning
        java.util.Collections.sort(possibleMoves, new Comparator<AiMove>() {
            @Override
            public int compare(AiMove a, AiMove b) {
                // -1 => a is better then b
                return b.weight - a.weight;
            }
        });


        AiMove bestMove = new AiMove(0, 999999); // really used only by the first layer
        int bestValue = maximizing ? -9999999 : 9999999;
        for (AiMove m : possibleMoves) { // try every piece
            int from = m.from;
            int to = m.to;


            Piece[] copyBoard = Board.copyBoard(board);

            //make the move
            Piece capture = copyBoard[to];
            copyBoard[to] = copyBoard[from];
            copyBoard[from] = null;
            copyBoard[to].moved();

            if (m.promotion != null) {
                copyBoard[to] = m.promotion;
            }

            //continue recursion
            AiMove move = minimax(copyBoard, depth + 1, maxDepth, !maximizing, alpha, beta, false);
            int newValue = move.value;

            if (maximizing) {
                if (newValue > bestValue) {
                    bestValue = newValue;
                    bestMove.set(from, to, capture, m.promotion, bestValue, move.aiDepth, move);
//                    if (first)
//                        Log.d("PlayAI", "Depth=" + depth + "; New best white: " + bestMove.toString());
                }
                if (newValue == bestValue) { // if equal there is a random chance it will choose this one
                    if (move.aiDepth < bestMove.aiDepth && first) {
                        if (depth == 0)
//                            Log.d("PlayAI", "Preferring move: '" + move.toStringShort() + "' over '" + bestMove.toStringShort() + "'");
                        bestMove.set(from, to, capture, m.promotion, bestValue, move.aiDepth, move);
                    } else if (randomIfEqual) {
                        if (random.nextInt(randomIfEqualChance) == 0) {
//                            Log.d("PlayAI", "Found new random move for white: " + bestMove.toString());
                            bestMove.set(from, to, capture, m.promotion, bestValue, move.aiDepth, move);
                        }
                    }
                }

                if (bestValue > alpha) alpha = bestValue;
            } else {
                if (newValue < bestValue) {
                    bestValue = newValue;
                    bestMove.set(from, to, capture, m.promotion, bestValue, move.aiDepth, move);
//                    if (first)
//                        Log.d("PlayAI", "Depth=" + depth + "; New best black: " + bestMove.toString());
                }
                if (newValue == bestValue) { // if equal there is a random chance it will choose this one
                    if (move.aiDepth < bestMove.aiDepth && first) {
                        if (depth == 0)
//                            Log.d("PlayAI", "Preferring move: '" + move.toStringShort() + "' over '" + bestMove.toStringShort() + "'");
                        bestMove.set(from, to, capture, m.promotion, bestValue, move.aiDepth, move);
                    } else if (randomIfEqual) {
                        if (random.nextInt(randomIfEqualChance) == 0) {
                            bestMove.set(from, to, capture, m.promotion, bestValue, move.aiDepth, move);
//                            Log.d("PlayAI", "Found new random move for black: " + bestMove.toString());
                        }
                    }
                }

                if (bestValue < beta) beta = bestValue;
            }
            if (beta <= alpha) break; // alpha-beta pruning

        }
        return bestMove;
    }

    @NonNull
    // generate list of all possible moves
    private static ArrayList<AiMove> generatePossibleMoves(Piece[] board, int depth, boolean maximizing) {

        final boolean[] attackedTiles = Board.getAttackedTiles(!maximizing, board, true);

        ArrayList<AiMove> possibleMoves = new ArrayList<>();

        for (int from = 0; from < Board.LEN; from++) { // try every piece
            if (board[from] != null) {
                if (board[from].isWhite() == maximizing) {
                    for (int to = 0; to < Board.LEN; to++) { // try every possible move
                        if (Board.isMoveValid(from, to, board, maximizing)) {

                            boolean promotion = board[from] instanceof Pawn && to / 8 == (board[from].isWhite() ? 0 : 7);

                            int moveWeight = 0;

                            if (board[to] != null) { // capture
                                moveWeight += CAPTURE_WEIGHT * board[to].getPieceValue() - board[from].getPieceValue();
                            }

                            if (attackedTiles[to]) { // stepping on square attacked byt the enemy is bad
                                moveWeight -= CAPTURE_WEIGHT;
                            }

                            if (promotion) {
                                moveWeight += PROMOTION_WEIGHT;
                                possibleMoves.add(new AiMove(from, to, board[to], new Queen(maximizing), 0, depth, moveWeight+18));
                                possibleMoves.add(new AiMove(from, to, board[to], new Rook(maximizing, 1), 0, depth, moveWeight+10));
                                possibleMoves.add(new AiMove(from, to, board[to], new Bishop(maximizing), 0, depth, moveWeight+6));
                                possibleMoves.add(new AiMove(from, to, board[to], new Knight(maximizing), 0, depth, moveWeight+6));
                            } else {
                                possibleMoves.add(new AiMove(from, to, board[to], null, 0, depth, moveWeight));
                            }
                        }
                    }
                }
            }
        }
        return possibleMoves;
    }

    public static Move[] openings = new Move[]{
            new Move("e2", "e4", null, null, null),
            new Move("d2", "d4", null, null, null)
    };

    public static Move playOpening (Piece[] board, boolean whitePlays) {
        if (whitePlays) { // the AI is the first to make a move
            return openings[random.nextInt(openings.length)];
        } else {
            return null; //TODO: react to opening
        }

    }

}
