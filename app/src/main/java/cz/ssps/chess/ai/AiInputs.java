package cz.ssps.chess.ai;

import cz.ssps.chess.chess.Board;
import cz.ssps.chess.pieces.Piece;

public class AiInputs {
    private final Board board;
    private final int aiDepth;

    public AiInputs(Board board, int aiDepth) {
        this.board = board;
        this.aiDepth = aiDepth;
    }

    public Piece[] getBoard () {
        return board.board;
    }

    public boolean whitePlays () {
        return board.whitePlays;
    }

    public int getAiDepth () {
        return aiDepth;
    }
}
