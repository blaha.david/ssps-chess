package cz.ssps.chess.ai;

import androidx.annotation.NonNull;

import cz.ssps.chess.chess.Move;
import cz.ssps.chess.pieces.Piece;

public class AiMove extends Move {
    public int aiDepth; // used only for the AI
    public int value;
    public int weight; // how good this move is
    public AiMove followingBestMove;

    public AiMove(int value, int aiDepth) {
        this.value = value;
        this.aiDepth = aiDepth;
        followingBestMove = null;
    }
    public AiMove (int from, int to, Piece take, Piece promotion, int value, int aiDepth, int weight) {
        this.from = from;
        this.to = to;
        this.capture = take;
        this.value = value;
        this.promotion = promotion;
        this.aiDepth = aiDepth;
        this.weight = weight;
    }
    public void set (int from, int to, Piece take, Piece promotion, int value, int aiDepth, AiMove previous) {
        this.from = from;
        this.to = to;
        this.capture = take;
        this.value = value;
        this.promotion = promotion;
        this.aiDepth = aiDepth;
        this.followingBestMove = previous;
    }

    @NonNull
    @Override
    public String toString() {
        return super.toString() + " (value=" + value + (aiDepth != -1 ? (", depth=" + aiDepth + ")") : ")") +
                ((followingBestMove != null) ? ("\n  ->  Following move: " + followingBestMove.toString()) : ".");
    }
    @NonNull
    public String toStringShort() {
        return super.toString() + " (value=" + value + (aiDepth != -1 ? (", depth=" + aiDepth + ")") : ")");
    }
}
