package cz.ssps.chess.ai;

import android.os.AsyncTask;
import android.util.Log;

public class AsyncAI extends AsyncTask<AiInputs, Integer, AiMove> {
    private final OnTaskCompleted listener;

    public AsyncAI(OnTaskCompleted listener) {
        this.listener = listener;
    }

    @Override
    protected AiMove doInBackground(AiInputs... aiInputs) {
        return AI.minimax(aiInputs[0]);
    }

    @Override
    protected void onPostExecute(AiMove aiMove) {
        super.onPostExecute(aiMove);
        if (aiMove != null) {
            listener.onTaskCompleted(aiMove);
        }else {
            Log.e("AI", "No move returned");
        }
    }


    public interface OnTaskCompleted{
        void onTaskCompleted(AiMove result);
    }
}
