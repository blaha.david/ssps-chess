package cz.ssps.chess.app;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import cz.ssps.chess.R;
import cz.ssps.chess.chess.ChessBoardView;

public class ChessFragment extends Fragment {
    private static final int OPTIONS_REQUEST = 0;

    private ChessBoardView chessBoard;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);//Make sure you have this line of code.
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_chess, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        chessBoard = view.findViewById(R.id.chessBoard);
        TextView textScore = view.findViewById(R.id.textScore);
//        TextView inputAiDepth = view.findViewById(R.id.inputAiDepth);
        LinearLayout layoutMovesHistory = view.findViewById(R.id.layoutMovesHistory);
        Bundle args = getArguments();

        SharedPreferences settings = getActivity().getSharedPreferences("Chess", 0);

        int saveMode = args.getInt("save", -1);
        String saveStr = "";
        if (saveMode == -1) {
            String pgn = settings.getString("save", "");
            if (pgn.equals("")) {
                Log.w("Load", "No save found");
                return;
            }
            saveStr = pgn;
        } else if (saveMode == -2) {
            saveStr = args.getString("fen");
        } else if (saveMode == -3) {
            saveStr = args.getString("pgn");
        }

        int mode = args.getInt("mode", 0);

        chessBoard.init(textScore, layoutMovesHistory, mode, saveMode, saveStr);

        if (saveMode >= 0) {
            chessBoard.restart();
        } else if (mode == 2) {
            chessBoard.playAsyncAI();
        }

//        view.findViewById(R.id.btnPlayAI).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                chessBoardView.playAI();
//            }
//        });
        view.findViewById(R.id.btnUndo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chessBoard.undoMove();
            }
        });
        view.findViewById(R.id.btnRedo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chessBoard.redoMove();
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // Inflate the menu; this adds items to the action bar if it is present.
        inflater.inflate(R.menu.menu_game, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        MainActivity activity = (MainActivity) getActivity();

        if (id == R.id.action_settings) {
            launchSettingsActivity();
            return true;
        }
        if (id == R.id.action_exit) {
            activity.finish();
            return true;
        }
        if (id == R.id.action_restart) {
            chessBoard.restart();
            return true;
        }
        if (id == R.id.action_about) {
            activity.displayAboutPopup();
            return true;
        }
        if (id == R.id.action_savefen) {
            activity.displaySavePopup(chessBoard.saveFEN());
            return true;
        }
        if (id == R.id.action_savepgn) {
            activity.displaySavePopup(chessBoard.savePGN());
            return true;
        }
//        if (id == R.id.action_sandbox) {
//            item.setChecked(!item.isChecked());
//            chessBoard.sandboxMode = item.isChecked();
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPause() {
        super.onPause();

        if (chessBoard.changed()) {
            SharedPreferences settings = getActivity().getSharedPreferences("Chess", 0);
            SharedPreferences.Editor editor = settings.edit();
            editor.putString("save", chessBoard.savePGN());
            editor.apply();
        }
    }

    private void launchSettingsActivity() {
        Intent intent = new Intent(getActivity(), SettingsActivity.class);
//        intent.putExtra("width", board.getGridWidth());
//        intent.putExtra("height", board.getGridHeight());
//        intent.putExtra("maxPoints", board.getMaxPoints());
//        intent.putExtra("color1", board.getColor1());
//        intent.putExtra("color2", board.getColor2());
        startActivityForResult(intent, OPTIONS_REQUEST);
    }

}