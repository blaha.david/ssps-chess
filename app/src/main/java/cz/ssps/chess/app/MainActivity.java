package cz.ssps.chess.app;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.QuickContactBadge;
import android.widget.TextView;
import android.widget.Toast;

import cz.ssps.chess.R;
import cz.ssps.chess.chess.ChessBoardView;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    public void displayAboutPopup () {

        // inflate the layout of the popup window
        View popupView = getLayoutInflater().inflate(R.layout.popup_about, null);

        // create the popup window
        int width = LinearLayout.LayoutParams.WRAP_CONTENT;
        int height = LinearLayout.LayoutParams.WRAP_CONTENT;
        final PopupWindow popupWindow = new PopupWindow(popupView, width, height, false);
        popupWindow.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        popupWindow.setOutsideTouchable(true);
        popupWindow.setFocusable(true);
        popupWindow.setElevation(50); // add drop shadow

        // show the popup window
        popupWindow.showAtLocation(popupView, Gravity.CENTER, 0, 0);

        // dismiss the popup window when touched
        popupView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                popupWindow.dismiss();
                return true;
            }
        });
    }

    public void displaySavePopup(String save) {

        // inflate the layout of the popup window
        View popupView = getLayoutInflater().inflate(R.layout.popup_fensave, null);

        // create the popup window
        int width = LinearLayout.LayoutParams.WRAP_CONTENT;
        int height = LinearLayout.LayoutParams.WRAP_CONTENT;
        final PopupWindow popupWindow = new PopupWindow(popupView, width, height, false);
        popupWindow.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        popupWindow.setOutsideTouchable(true);
        popupWindow.setFocusable(true);
        popupWindow.setElevation(50); // add drop shadow

        ((TextView)popupView.findViewById(R.id.textFen)).setText(save);

        final String text = save;
        ((Button)popupView.findViewById(R.id.btnCopySave)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ClipboardManager clipManager = (ClipboardManager)getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("Label", text);
                clipManager.setPrimaryClip(clip);
                Toast.makeText(MainActivity.this, "Zkopírováno do schránky", Toast.LENGTH_SHORT).show();
            }
        });


        // show the popup window
        popupWindow.showAtLocation(popupView, Gravity.CENTER, 0, 0);

        // dismiss the popup window when touched
        popupView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                popupWindow.dismiss();
                return true;
            }
        });
    }
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        if (requestCode == OPTIONS_REQUEST) {
//            if (resultCode == RESULT_OK) {
////                int width = data.getIntExtra("width", -1);
////                int height = data.getIntExtra("height", -1);
////                int maxPoints = data.getIntExtra("maxPoints", -1);
////                int color1 = data.getIntExtra("color1", -1);
////                int color2 = data.getIntExtra("color2", -1);
////                if (width != -1 && height != -1 && maxPoints != -1) {
////                    board.setMaxPoints(maxPoints);
////                    board.newBoard(width, height);
////                }
////                if (color1 != -1 && color2 != -1) {
////                    board.setColor1(color1);
////                    board.setColor2(color2);
////                }
//            }
//        }
//        super.onActivityResult(requestCode, resultCode, data);
//    }

    @Override
    public void onBackPressed()
    {
        ChessBoardView board = (ChessBoardView) findViewById(R.id.chessBoard);

        if (board != null) {
            if (board.asyncAiTask != null)
                board.asyncAiTask.cancel(true);
        }

        super.onBackPressed();
    }
}