package cz.ssps.chess.app;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import cz.ssps.chess.R;


public class MenuFragment extends Fragment {
    private static final int OPTIONS_REQUEST = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);//Make sure you have this line of code.
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_menu, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final MainActivity activity = (MainActivity) getActivity();

        final Spinner spinnerMode = view.findViewById(R.id.spinnerMode);

        final SharedPreferences settings = getActivity().getSharedPreferences("Chess", 0);
        final String save = settings.getString("save", "");

        view.findViewById(R.id.btnNewGame).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putInt("save", 0); // -2 means load from FEN
                bundle.putInt("mode", spinnerMode.getSelectedItemPosition());
                NavHostFragment.findNavController(MenuFragment.this)
                        .navigate(R.id.action_new_game, bundle);
            }
        });
        view.findViewById(R.id.btnLoadGame).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (save.equals("")) {
                    Toast.makeText(getContext(), "Žádná uložená hra", Toast.LENGTH_SHORT).show();
                } else {
                    Bundle bundle = new Bundle();
                    bundle.putInt("save", -1); // -2 means load from FEN
                    bundle.putInt("mode", spinnerMode.getSelectedItemPosition());
                    NavHostFragment.findNavController(MenuFragment.this)
                            .navigate(R.id.action_new_game, bundle);
                }
            }
        });
        view.findViewById(R.id.btnLoadFEN).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ClipboardManager clipManager = (ClipboardManager) activity.getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = clipManager.getPrimaryClip();
                if (clip.getItemCount() == 0) {
                    Toast.makeText(getContext(), "Zkopírujte FEN do schránky", Toast.LENGTH_SHORT).show();
                    return;
                }
                String fen = clip.getItemAt(0).getText().toString();
                Bundle bundle = new Bundle();
                bundle.putInt("save", -2); // -2 means load from FEN
                bundle.putInt("mode", spinnerMode.getSelectedItemPosition());
                bundle.putString("fen", fen);
                NavHostFragment.findNavController(MenuFragment.this)
                        .navigate(R.id.action_new_game, bundle);
            }
        });
        view.findViewById(R.id.btnLoadPGN).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ClipboardManager clipManager = (ClipboardManager) activity.getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = clipManager.getPrimaryClip();
                if (clip.getItemCount() == 0) {
                    Toast.makeText(getContext(), "Zkopírujte PGN do schránky", Toast.LENGTH_SHORT).show();
                    return;
                }
                String pgn = clip.getItemAt(0).getText().toString();
                Bundle bundle = new Bundle();
                bundle.putInt("save", -3); // -3 means load from PGN
                bundle.putInt("mode", spinnerMode.getSelectedItemPosition());
                bundle.putString("pgn", pgn);
                NavHostFragment.findNavController(MenuFragment.this)
                        .navigate(R.id.action_new_game, bundle);
            }
        });
        view.findViewById(R.id.btnSettings).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                launchSettingsActivity();
            }
        });

        view.findViewById(R.id.btnExit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().finish();
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // Inflate the menu; this adds items to the action bar if it is present.
        inflater.inflate(R.menu.menu_main, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        MainActivity activity = (MainActivity) getActivity();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            launchSettingsActivity();
            return true;
        }
        if (id == R.id.action_exit) {
            activity.finish();
            return true;
        }
        if (id == R.id.action_about) {
            activity.displayAboutPopup();
        }

        return super.onOptionsItemSelected(item);
    }

    private void launchSettingsActivity() {
        Intent intent = new Intent(getActivity(), SettingsActivity.class);
        startActivityForResult(intent, OPTIONS_REQUEST);
    }

}