package cz.ssps.chess.chess;

import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import cz.ssps.chess.pieces.King;
import cz.ssps.chess.pieces.Pawn;
import cz.ssps.chess.pieces.Piece;

public class Board {
    public static final int SIZE = 8;
    public static final int LEN = 64;

    public Piece[] board;

    public final boolean[] validTiles = new boolean[64];
    public int selectedIndex = -1;
    public boolean whitePlays = true;

    public Winner winner;
    public int mode;
    public ArrayList<Move> moveHistory = new ArrayList<>();

    public Board(int mode) {
        this.board = new Piece[LEN];
        this.mode = mode;
        this.winner = Winner.None;
    }

    public Board(Piece[] board, boolean whitePlays, int mode) {
        this.board = board;
        this.whitePlays = whitePlays;
        this.winner = Winner.None;
        this.mode = mode;
    }

    public Board(Piece[] board, boolean whitePlays, ArrayList<Move> moveHistory, int mode) {
        this.board = board;
        this.whitePlays = whitePlays;
        this.moveHistory = moveHistory;
        this.winner = Winner.None;
        this.mode = mode;
    }
    public Board(Piece[] board, boolean whitePlays, Move[] moveHistory, Winner winner, int mode) {
        this.board = board;
        this.whitePlays = whitePlays;
        this.moveHistory = new ArrayList<>(moveHistory.length);
        this.moveHistory.addAll(Arrays.asList(moveHistory));
        this.winner = winner;
        this.mode = mode;
    }

    public void move(Move move, boolean playerMove, ChessCallbacks chessCallbacks) {
        Piece take = board[move.to];

        board = makeMove(move.from, move.to, board, whitePlays); // replace the old board with the new board

        // detect if pawn is promoting - is on the last/first row
        if (playerMove) {
            Piece p = getPiece(move.to);
            if ((move.to / 8 == 0 && p.isWhite()) || (move.to / 8 == 7 && !p.isWhite())) {
                if (p instanceof Pawn) {
                    chessCallbacks.onPromotion(whitePlays, move.from, move.to, take);
                    return; // wait for user move.to select a promotion piece - continue in finishPromotion() function
                }
            }
        } else {
            if (move.promotion != null) {
                board[move.to] = move.promotion;
            }
        }

        moveHistory.add(move);
        whitePlays = !whitePlays;
        selectedIndex = -1;
    }

    public void finishPromotion(int from, int to, Piece take, Piece promotion) {
        moveHistory.add(new Move(from, to, board[to], take, promotion));
        board[to] = promotion;
        whitePlays = !whitePlays;
        selectedIndex = -1;
    }

    // check if the move is really valid
    // can only move to stop a check
    // cannot castle while in check
    // cannot castle through a checked tile
    public static boolean isMoveValid(int from, int to, Piece[] board, boolean whitePlays) {
        return isMoveValid(from, to, board, whitePlays, false);
    }

    private static boolean isMoveValid(int from, int to, Piece[] board, boolean whitePlays, boolean dontTryMakeMove) {
        if (board[from] != null) {

            if (board[from].isWhite() != whitePlays) return false;

            if (board[from].validMove(from, to, board)) {
                if (!dontTryMakeMove) {
                    Piece[] newBoard = makeMove(from, to, board, whitePlays);
//                    Log.d("DEBUG", "trying move from " + from + " to " + to + (whitePlays ? " (white)" : " (black)"));
                    if (isKingChecked(whitePlays, newBoard, true)) {
                        return false;
                    }

                    // King cannot castle through a checked square
                    //castling:
                    int wki = whiteKingIndex(board);
                    int bki = blackKingIndex(board);
                    if (whitePlays && from == wki && wki == King.WHITE_KING) { // white
                        if (!board[from].hasMoved() && to == King.LEFT_WHITE_CASTLE) { // left castle
                            boolean[] attackedTiles = getAttackedTiles(false, board, true);
                            return !attackedTiles[King.LEFT_WHITE_CASTLE + 1] && !attackedTiles[wki];
                        }
                        if (!board[from].hasMoved() && to == King.RIGHT_WHITE_CASTLE) { // right castle
                            boolean[] attackedTiles = getAttackedTiles(false, board, true);
                            return !attackedTiles[King.RIGHT_WHITE_CASTLE - 1] && !attackedTiles[wki];
                        }
                    } else if (!whitePlays && from == bki && bki == King.BLACK_KING) { // black
                        if (!board[from].hasMoved() && to == King.LEFT_BLACK_CASTLE) { // left castle
                            boolean[] attackedTiles = getAttackedTiles(true, board, true);
                            return !attackedTiles[King.LEFT_BLACK_CASTLE + 1] && !attackedTiles[bki];
                        }
                        if (!board[from].hasMoved() && to == King.RIGHT_BLACK_CASTLE) { // right castle
                            boolean[] attackedTiles = getAttackedTiles(true, board, true);
                            return !attackedTiles[King.RIGHT_BLACK_CASTLE - 1] && !attackedTiles[bki];
                        }
                    }
                }
                return true;
            }
        }
        return false;
    }

    public static boolean isKingChecked(boolean white, Piece[] board, boolean dontTryMakeMove) { // if the opposite player is attacking the tile the king is on
        if (white) {
            int wki = whiteKingIndex(board);
            return isTileAttacked(wki, false, board, dontTryMakeMove);
        } else {
            int bki = blackKingIndex(board);
            return isTileAttacked(bki, true, board, dontTryMakeMove);
        }
    }

    public static boolean[] getAttackedTiles(boolean white, Piece[] board, boolean dontTryMakeMove) {
        boolean[] attackedTiles = new boolean[LEN];
        for (int from = 0; from < board.length; from++) {
            if (board[from] != null) {
                if (board[from].isWhite() == white) {
                    for (int to = 0; to < board.length; to++) {
                        if (board[from] instanceof Pawn) {
                            Pawn pawn = (Pawn) board[from];
                            if (pawn.isTileAttacked(from, to, board)) {
                                attackedTiles[to] = true;
                            }
                        } else {
                            if (isMoveValid(from, to, board, white, dontTryMakeMove)) {
                                attackedTiles[to] = true;
                            }
                        }
                    }
                }
            }
        }
        return attackedTiles;
    }

    // white=true -> if white is attacking that tile (attacking black pieces)
    public static boolean isTileAttacked(int tile, boolean white, Piece[] board, boolean dontTryMakeMove) {
        for (int i = 0; i < board.length; i++) {
            if (board[i] != null) {
                if (board[i].isWhite() == white) {
                    if (board[i] instanceof Pawn) {
                        Pawn pawn = (Pawn) board[i];
                        if (pawn.isTileAttacked(i, tile, board)) {
                            return true;
                        }
                    } else {
                        if (isMoveValid(i, tile, board, white, dontTryMakeMove)) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    public static Piece[] makeMove(int from, int to, Piece[] board, boolean whitePlays) {
        int whiteKingIndex = whiteKingIndex(board);
        int blackKingIndex = blackKingIndex(board);

        Piece[] newBoard = copyBoard(board);

        //castling:
        if (whitePlays && from == whiteKingIndex && whiteKingIndex == King.WHITE_KING) { // white
            if (!newBoard[from].hasMoved() && to == King.LEFT_WHITE_CASTLE) { // left castle
                // move the rook:
                newBoard[Piece.LEFT_WHITE_ROOK + 3] = newBoard[Piece.LEFT_WHITE_ROOK];
                newBoard[Piece.LEFT_WHITE_ROOK] = null;
            }
            if (!newBoard[from].hasMoved() && to == King.RIGHT_WHITE_CASTLE) { // right castle
                // move the rook:
                newBoard[Piece.RIGHT_WHITE_ROOK - 2] = newBoard[Piece.RIGHT_WHITE_ROOK];
                newBoard[Piece.RIGHT_WHITE_ROOK] = null;
            }
        }
        if (!whitePlays && from == blackKingIndex && blackKingIndex == King.BLACK_KING) { // black
            if (!newBoard[from].hasMoved() && to == King.LEFT_BLACK_CASTLE) { // left castle
                // move the rook:
                newBoard[Piece.LEFT_BLACK_ROOK + 3] = newBoard[Piece.LEFT_BLACK_ROOK];
                newBoard[Piece.LEFT_BLACK_ROOK] = null;
            }
            if (!newBoard[from].hasMoved() && to == King.RIGHT_BLACK_CASTLE) { // right castle
                // move the rook:
                newBoard[Piece.RIGHT_BLACK_ROOK - 2] = newBoard[Piece.RIGHT_BLACK_ROOK];
                newBoard[Piece.RIGHT_BLACK_ROOK] = null;
            }
        }
        Pawn pawn = null;
        if (newBoard[from] instanceof Pawn) {
            pawn = (Pawn) newBoard[from];
        }

        // en passant - take piece:
        if (pawn != null) {
            if (pawn.canEnPassant(from, to, newBoard)) {
                newBoard[to + (whitePlays ? 8 : -8)] = null; // remove the neighbouring pawn
            }
        }

        //set en passant flag:
        ChessBoardView.resetEnPassant(newBoard, whitePlays); // remove en passant flag from all same colored pawns
        if (pawn != null) {
            if (pawn.canMoveTwice(from, to, newBoard)) {
                pawn.setEnPassantFlag();
            }
        }


        // MOVE THE PIECE:
        newBoard[to] = newBoard[from];
        newBoard[from] = null;
        newBoard[to].moved();

        return newBoard;
    }

    public void undoMove() {
        if (moveHistory.size() == 0) return;
        Move lastMove = moveHistory.remove(moveHistory.size() - 1);

        board[lastMove.from] = board[lastMove.to];
        board[lastMove.to] = lastMove.capture;

        whitePlays = !whitePlays;


        Piece piece = board[lastMove.from];
        piece.undoMove();
        if (piece instanceof Pawn) {
            Pawn pawn = (Pawn) piece;

        }
    }

    public void makeMove(int from, int to) {
        board[to] = board[from];
        board[from] = null;
        board[to].moved();
    }

    public void setPiece(int index, Piece piece) {
        if (index < 0 || index >= LEN) return;

        board[index] = piece;
    }

    public Piece getPiece(int index) {
        if (index >= board.length || index < 0) {
            return null;
        }
        return board[index];
    }

    // calculate which player has advantage, also used as node heuristic value for minimax
    public static int getScore(Piece[] board) {
        int sum = 0;
        for (Piece piece : board) {
            if (piece != null) {
                if (piece.isWhite()) {
                    sum += piece.getPieceValue();
                } else {
                    sum -= piece.getPieceValue();
                }
            }
        }
        return sum;
    }

    public Move lastMove() {
        if (moveHistory.size() == 0) return new Move();

        return moveHistory.get(moveHistory.size() - 1);
    }

    public static int whiteKingIndex(Piece[] board) {
        for (int i = 0; i < LEN; i++) {
            if (board[i] instanceof King) {
                if (board[i].isWhite()) {
                    return i;
                }
            }
        }
        Log.w("DEBUG", "ERROR: white king not found");
        return -1;
    }

    public static int blackKingIndex(Piece[] board) {
        for (int i = 0; i < LEN; i++) {
            if (board[i] instanceof King) {
                if (!board[i].isWhite()) {
                    return i;
                }
            }
        }
        Log.w("DEBUG", "ERROR: black king not found");
        return -1;
    }

    public static Winner checkForWinner(Piece[] board, boolean whitePlays) {
        boolean whiteCanMove = false;
        boolean blackCanMove = false;
        boolean onlyTwoKings = true;
        for (int from = 0; from < LEN; from++) {
            if (board[from] != null) {
                if (!(board[from] instanceof King)) {
                    onlyTwoKings = false;
                }
            }
        }
        if (onlyTwoKings) {
            return Winner.Draw;
        }
        for (int from = 0; from < LEN; from++) {
            if (whiteCanMove && blackCanMove) {
                break;
            }
            for (int to = 0; to < LEN; to++) {
                if (isMoveValid(from, to, board, true)) {
                    whiteCanMove = true;
                }
                if (isMoveValid(from, to, board, false)) {
                    blackCanMove = true;
                }
                if (whiteCanMove && blackCanMove) {
                    break;
                }
            }
        }

        if (whitePlays) {
            if (!whiteCanMove) {
                if (isKingChecked(true, board, true)) {
                    return Winner.Black;
                }
                return Winner.Draw;
            }
        } else {
            if (!blackCanMove) {
                if (isKingChecked(false, board, true)) {
                    return Winner.White;
                }
                return Winner.Draw;
            }
        }

        return Winner.None;
    }

    public static Piece[] copyBoard(Piece[] grid) {
        Piece[] newGrid = new Piece[LEN];

        for (int i = 0; i < LEN; i++) {
            if (grid[i] != null) {
                newGrid[i] = grid[i].copy();
            }
        }

        return newGrid;
    }

    public Board clone() {
        return new Board(copyBoard(board), whitePlays, moveHistory.toArray(new Move[moveHistory.size()]), winner, mode);
    }

    public String saveFEN() {
        return FEN.boardToFEN(board, whitePlays);
    }

    public enum Winner {
        None,
        White,
        Black,
        Draw,
    }
}