package cz.ssps.chess.chess;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.preference.PreferenceManager;

import cz.ssps.chess.R;
import cz.ssps.chess.ai.AI;
import cz.ssps.chess.ai.AiInputs;
import cz.ssps.chess.ai.AiMove;
import cz.ssps.chess.ai.AsyncAI;
import cz.ssps.chess.pieces.*;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

import java.util.ArrayList;
import java.util.List;

public class ChessBoardView extends View {

    public static final String initialPosition = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1";
    public static final String testingPosition = "r2k3r/p4p1p/8/3P4/6P1/3N4/Q7/6K1 b - - 0 1"; // AI benchmark
//    public static final String testingPosition = "8/2p5/8/KP5r/8/7k/8/8 b - - 0 1"; // promotion
//    public static final String testingPosition = "rnbqkbnr/8/8/8/8/8/8/R3K2R b KQkq - 0 1"; // castling
//    public static final String testingPosition = "k7/8/8/8/8/4K3/8/8 w - - 0 1";

    public static final String fenWin = "rk2r3/p4p1p/Q7/2NP4/6P1/8/8/6K1 w - - 0 1";

    public static final String pgn = "[Event \"Chess\"]\n" +
            "[Site \"Chess app\"]\n" +
            "[Date \"2022.01.15\"]\n" +
            "[EventDate \"?\"]\n" +
            "[Round \"?\"]\n" +
            "[Result \"*\"]\n" +
            "[White \"Player1\"]\n" +
            "[Black \"Player2\"]\n" +
            "\n" +
            "1. d2d4 2. c7c5 3. Ng1f3 4. xc5d4 5. Nxf3d4 6. Qd8a5 7. Qd1d2 8. Qxa5d2 9. Bxc1d2 10. b7b5 11. Nxd4b5 12. Ng8h6 13. Bxd2h6 14. g7g5 15. Bxh6f8 16. Kxe8f8 17. Nxb5a7 18. g5g4 19. Nxa7c8 20. g4g3 21. xf2g3 22. d7d6 23. Nxc8d6 24. xe7d6 25. Nb1c3 26. d6d5 27. Nxc3d5 28. Nb8c6 29. c2c3 30. Nc6b4 31. xc3b4 32. Ra8c8 33. b4b5 34. Rc8c6 35. xb5c6 36. Kf8g8 37. c6c7 38. Kg8f8";

    public static final String testingPromotionPosition = "N7/4k2P/7R/8/8/8/N7/3K4 w - - 0 1";
    private Board board;

    private TextView textScore;
    private LinearLayout layoutMovesHistory;

    private int mode; // 0=player vs player, 1=player vs AI, 2=AI vs AI
    private boolean whiteStarts;
    private int aiDepth;

    private final Paint bgWhite, bgBlack, bgSelected, bgValid, bgHighlight, bgChecked, bgDebug, textWhite, textBlack;
    private final Rect tile;

    private final Bitmap[] pieceImages;

    private final float strokeWidth = 14f;

    private final Context _context;
    private PopupWindow promotionDialog;

    private boolean waitingForPromotionDialog = false;
    private boolean waitingForAsyncAI = false;
    private boolean gameOver = false;
    public AsyncAI asyncAiTask;

    public boolean sandboxMode = false; // can make whatever move

    public ArrayList<Board> history = new ArrayList<Board>();
    public int historyIndex = -1;

    public ChessBoardView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this._context = context;

        TypedArray theme = context.getTheme().obtainStyledAttributes(attrs, R.styleable.ChessBoard, 0, 0);
        theme.recycle();

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        int bgWhiteColor = Color.parseColor(preferences.getString("white_color", "#E6BF83"));
        int bgBlackColor = Color.parseColor(preferences.getString("black_color", "#7E510D"));

        whiteStarts = preferences.getString("starting_player", "white").equals("white");
        aiDepth = Integer.parseInt(preferences.getString("ai_difficulty", "5"));

        bgWhite = new Paint();
//        bgWhite.setColor(ContextCompat.getColor(context, R.color.bgWhite));
        bgWhite.setColor(bgWhiteColor);
        bgWhite.setStyle(Paint.Style.FILL);
        bgBlack = new Paint();
//        bgBlack.setColor(ContextCompat.getColor(context, R.color.bgBlack));
        bgBlack.setColor(bgBlackColor);
        bgBlack.setStyle(Paint.Style.FILL);
        bgSelected = new Paint();
        bgSelected.setColor(ContextCompat.getColor(context, R.color.bgSelected));
        bgSelected.setStyle(Paint.Style.FILL);
        bgValid = new Paint();
        bgValid.setColor(ContextCompat.getColor(context, R.color.bgValid));
        bgValid.setStyle(Paint.Style.FILL);
        bgDebug = new Paint();
        bgDebug.setColor(ContextCompat.getColor(context, R.color.bgDebug));
        bgDebug.setStyle(Paint.Style.FILL);
        bgHighlight = new Paint();
        bgHighlight.setColor(ContextCompat.getColor(context, R.color.bgHighlight));
        bgHighlight.setStyle(Paint.Style.STROKE);
        bgHighlight.setStrokeWidth(strokeWidth);
        bgChecked = new Paint();
        bgChecked.setColor(ContextCompat.getColor(context, R.color.bgChecked));
        bgChecked.setStyle(Paint.Style.STROKE);
        bgChecked.setStrokeWidth(strokeWidth);

        textWhite = new TextPaint();
//        textWhite.setColor(ContextCompat.getColor(context, R.color.bgWhite));
        textWhite.setColor(bgWhiteColor);
        textWhite.setStyle(Paint.Style.FILL);
        textWhite.setTextSize(40f);
        textBlack = new TextPaint();
//        textBlack.setColor(ContextCompat.getColor(context, R.color.bgBlack));
        textBlack.setColor(bgBlackColor);
        textBlack.setStyle(Paint.Style.FILL);
        textBlack.setTextSize(40f);

        tile = new Rect();

        pieceImages = new Bitmap[12];
        pieceImages[0] = BitmapFactory.decodeResource(context.getResources(), R.drawable.white_king);
        pieceImages[1] = BitmapFactory.decodeResource(context.getResources(), R.drawable.white_queen);
        pieceImages[2] = BitmapFactory.decodeResource(context.getResources(), R.drawable.white_bishop);
        pieceImages[3] = BitmapFactory.decodeResource(context.getResources(), R.drawable.white_knight);
        pieceImages[4] = BitmapFactory.decodeResource(context.getResources(), R.drawable.white_rook);
        pieceImages[5] = BitmapFactory.decodeResource(context.getResources(), R.drawable.white_pawn);
        pieceImages[6] = BitmapFactory.decodeResource(context.getResources(), R.drawable.black_king);
        pieceImages[7] = BitmapFactory.decodeResource(context.getResources(), R.drawable.black_queen);
        pieceImages[8] = BitmapFactory.decodeResource(context.getResources(), R.drawable.black_bishop);
        pieceImages[9] = BitmapFactory.decodeResource(context.getResources(), R.drawable.black_knight);
        pieceImages[10] = BitmapFactory.decodeResource(context.getResources(), R.drawable.black_rook);
        pieceImages[11] = BitmapFactory.decodeResource(context.getResources(), R.drawable.black_pawn);

        if (promotionDialog != null) {
            promotionDialog.dismiss();
            promotionDialog = null;
        }
        waitingForPromotionDialog = false;
    }

    public void restart() {
        if (asyncAiTask != null) asyncAiTask.cancel(true);

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(_context);
        whiteStarts = preferences.getString("starting_player", "white").equals("white");
        aiDepth = Integer.parseInt(preferences.getString("ai_difficulty", "5"));

        if (promotionDialog != null) {
            promotionDialog.dismiss();
            promotionDialog = null;
        }
        waitingForPromotionDialog = false;
        waitingForAsyncAI = false;

        gameOver = false;

        loadFEN(initialPosition, mode);
        board.whitePlays = whiteStarts;

        board.moveHistory.clear();
        history.clear();


        history.add(board.clone());
        historyIndex = 0;

        if (mode == 2) { // AI vs AI
            playAsyncAI();
        }
        updateUI();
    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        int tileSize = getWidth() / Board.SIZE;

//        boolean[] attackedTiles = Board.getAttackedTiles(false, board.board, true);

        for (int n = 0; n < Board.SIZE * Board.SIZE; n++) {
            int i = n % 8;
            int j = n / 8;
            tile.set(i * tileSize, j * tileSize, i * tileSize + tileSize, j * tileSize + tileSize);
            boolean blackTile = ((i % 2 == 0) ^ (j % 2 == 0));
            canvas.drawRect(tile, blackTile ? bgBlack : bgWhite);

            if (n % Board.SIZE == 0) { // left column
                canvas.drawText("" + (8 - n / Board.SIZE), tile.left + 2f, tile.top + 35f, blackTile ? textWhite : textBlack);
            }
            if (n / Board.SIZE == 7) { //bottom row
                canvas.drawText("" + (char) ((int) 'A' + (n % Board.SIZE)), tile.right - 30f, tile.bottom - 5f, blackTile ? textWhite : textBlack);
            }

            Piece piece = board.getPiece(n);
            if (piece != null) {
                if (board.selectedIndex == n) {
                    canvas.drawRect(tile, bgSelected);
                }
                canvas.drawBitmap(pieceImages[piece.getImageIndex()], null, tile, null);

//                if (piece instanceof Pawn) { // highlight en passant flagged pawn
//                    if (((Pawn)piece).hasEnPassantFlag()) {
//                        canvas.drawCircle(tile.centerX(), tile.centerY(), tile.width()/5f, bgDebug);
//                    }
//                }
            }

            if (board.validTiles[n]) {
                canvas.drawCircle(tile.centerX(), tile.centerY(), tile.width() / 5f, bgValid);
            }
//            if (attackedTiles[n]) {
//                canvas.drawCircle(tile.centerX(), tile.centerY(), tile.width() / 5f, bgDebug);
//            }

            tile.inset((int) strokeWidth / 2, (int) strokeWidth / 2);

            Move lastMove = board.lastMove();
            if (n == lastMove.from || n == lastMove.to) {
                canvas.drawRect(tile, bgHighlight);
            }
        }

        {
            int wki = Board.whiteKingIndex(board.board);
            int i = wki % 8;
            int j = wki / 8;
            tile.set(i * tileSize, j * tileSize, i * tileSize + tileSize, j * tileSize + tileSize);
            tile.inset((int) strokeWidth / 2, (int) strokeWidth / 2);
            if (Board.isKingChecked(true, board.board, true)) {
                canvas.drawRect(tile, bgChecked);
            }
        }
        {
            int bki = Board.blackKingIndex(board.board);
            int i = bki % 8;
            int j = bki / 8;
            tile.set(i * tileSize, j * tileSize, i * tileSize + tileSize, j * tileSize + tileSize);
            tile.inset((int) strokeWidth / 2, (int) strokeWidth / 2);
            if (Board.isKingChecked(false, board.board, true)) {
                canvas.drawRect(tile, bgChecked);
            }
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        if (waitingForPromotionDialog) return true;
        if (waitingForAsyncAI) return true;
        if (gameOver) return true;
        if (mode == 2) return true; // AI vs AI

        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            int tileSize = getWidth() / Board.SIZE;

            int x = (int) event.getX();
            int y = (int) event.getY();

            int i = x / tileSize;
            int j = y / tileSize;

            if (j >= Board.SIZE || i >= Board.SIZE || j < 0 || i < 0)
                return super.onTouchEvent(event); // ignore out of bound touches

            select(j * Board.SIZE + i);
            return true;
        }

        return super.onTouchEvent(event);
    }


    public void select(int index) {
        if (index == board.selectedIndex) { // deselect piece
            board.selectedIndex = -1;
            resetValidMoves();
        } else if (board.selectedIndex == -1) { // select piece
            if (board.getPiece(index) == null) {
                return;
            }
            if (board.whitePlays ^ board.getPiece(index).isWhite() && !sandboxMode) {
                return;
            }
            board.selectedIndex = index;
            displayValidMoves();
        } else { // move piece
            if (Board.isMoveValid(board.selectedIndex, index, board.board, board.whitePlays) || sandboxMode) {
                board.move(new Move(board.selectedIndex, index, board.getPiece(board.selectedIndex), board.getPiece(index), null), true, new ChessCallbacks() {
                    @Override
                    void onPromotion(boolean white, int from, int to, Piece take) {
                        showPromotionPopup(white, from, to, take);
                    }
                });

                Board.Winner win = Board.checkForWinner(board.board, board.whitePlays);
                board.winner = win;
                Log.d("DEBUG", "Winner=" + win.toString());
                showGameResultPopup(win);

                addBoardToHistory();

                if (mode == 1 && win == Board.Winner.None && !waitingForPromotionDialog) {
                    updateUI();
                    invalidate();
//                    playAI();
                    playAsyncAI();
                }
            }
            board.selectedIndex = -1;
            resetValidMoves();
            updateUI();
        }
        invalidate();
    }

    private void addBoardToHistory() {
        if (historyIndex < history.size() - 1) {
            history.subList(historyIndex + 1, history.size()).clear();
        }
        history.add(board.clone());
        historyIndex++;
    }

    @SuppressLint("SetTextI18n")
    public void updateUI() {

        if (textScore != null) {
            textScore.setText("Score:" + Board.getScore(board.board));
        }

        layoutMovesHistory.removeAllViewsInLayout();
        List<Move> moveHistory = history.get(history.size() - 1).moveHistory;
        int moveNumber = moveHistory.size();
        for (int i = moveHistory.size() - 1; i >= 0; i--) {
            String moveText = moveHistory.get(i).toNotation(moveNumber);
            final TextView textView = new TextView(this._context);
            textView.setText(moveText);
            if (i == historyIndex - 1) {
                textView.setTextColor(Color.RED);
            }
            layoutMovesHistory.addView(textView);
            moveNumber--;
        }
        invalidate();
    }

    public void playAsyncAI() {
        AI.nodes = 0; //reset counter
        waitingForAsyncAI = true;
        asyncAiTask = new AsyncAI(new AsyncAI.OnTaskCompleted() {
            @Override
            public void onTaskCompleted(AiMove move) {
                move.piece = board.getPiece(move.from);
                board.move(move, false, new ChessCallbacks() {
                    @Override
                    void onPromotion(boolean white, int from, int to, Piece take) {
                    }
                });

                Board.Winner win = Board.checkForWinner(board.board, board.whitePlays);
                board.winner = win;
//                Log.d("DEBUG", "Winner=" + win.toString());
                showGameResultPopup(win);

                addBoardToHistory();

                waitingForAsyncAI = false;
                updateUI();
                invalidate();

                if (mode == 2 && win == Board.Winner.None) { // AI vs AI
                    playAsyncAI();
                } else {
                    asyncAiTask = null;
                }
            }
        });
        asyncAiTask.execute(new AiInputs(board, aiDepth));
    }

    public void displayValidMoves() {
        for (int i = 0; i < Board.LEN; i++) {
            board.validTiles[i] = Board.isMoveValid(board.selectedIndex, i, board.board, board.whitePlays);
        }
        invalidate();
    }

    public void resetValidMoves() {
        for (int i = 0; i < Board.LEN; i++) {
            board.validTiles[i] = false;
        }
        invalidate();
    }

    public static void resetEnPassant(Piece[] board, boolean whitePlays) {
        for (int i = 0; i < Board.LEN; i++) {
            if (board[i] != null) {
                if (board[i] instanceof Pawn && board[i].isWhite() == whitePlays) {
                    ((Pawn) board[i]).removeEnPassantFlag();
                }
            }
        }
    }


    public void showPromotionPopup(final boolean white, final int from, final int to, final Piece take) {

        waitingForPromotionDialog = true;

        // inflate the layout of the popup window
        LayoutInflater inflater = (LayoutInflater) _context.getSystemService(LAYOUT_INFLATER_SERVICE);
        View popupView = inflater.inflate(R.layout.popup_promotion, null);

        // create the popup window
        int width = LinearLayout.LayoutParams.WRAP_CONTENT;
        int height = LinearLayout.LayoutParams.WRAP_CONTENT;
        final PopupWindow popupWindow = new PopupWindow(popupView, width, height, false);
        this.promotionDialog = popupWindow;
//        popupWindow.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        popupWindow.setOutsideTouchable(false);
        popupWindow.setElevation(50); // add drop shadow

        // show the popup window
        popupWindow.showAtLocation(this, Gravity.CENTER, 0, 0);

        Button btnQueen = popupView.findViewById(R.id.btnQueen);
        btnQueen.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                finishPromotion(from, to, take, new Queen(white));
                popupWindow.dismiss();
            }
        });
        Button btnRook = popupView.findViewById(R.id.btnRook);
        btnRook.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                finishPromotion(from, to, take, new Rook(white, 1));
                popupWindow.dismiss();
            }
        });
        Button btnBishop = popupView.findViewById(R.id.btnBishop);
        btnBishop.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                finishPromotion(from, to, take, new Bishop(white));
                popupWindow.dismiss();
            }
        });
        Button btnKnight = popupView.findViewById(R.id.btnKnight);
        btnKnight.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                finishPromotion(from, to, take, new Knight(white));
                popupWindow.dismiss();
            }
        });
        if (white) {
            btnQueen.setForeground(ContextCompat.getDrawable(_context, R.drawable.white_queen));
            btnRook.setForeground(ContextCompat.getDrawable(_context, R.drawable.white_rook));
            btnBishop.setForeground(ContextCompat.getDrawable(_context, R.drawable.white_bishop));
            btnKnight.setForeground(ContextCompat.getDrawable(_context, R.drawable.white_knight));
        } else { // change icons to black version (default are white)
            btnQueen.setForeground(ContextCompat.getDrawable(_context, R.drawable.black_queen));
            btnRook.setForeground(ContextCompat.getDrawable(_context, R.drawable.black_rook));
            btnBishop.setForeground(ContextCompat.getDrawable(_context, R.drawable.black_bishop));
            btnKnight.setForeground(ContextCompat.getDrawable(_context, R.drawable.black_knight));
        }

    }

    public void finishPromotion(int from, int to, Piece take, Piece promotion) {

        board.finishPromotion(from, to, take, promotion);

        waitingForPromotionDialog = false;
        promotionDialog = null;


        if (mode == 1) {
            updateUI();
            invalidate();
            playAsyncAI();
        }

        updateUI();
        invalidate();
    }


    // Display popup dialog with game result and buttons for dismissing the popup and restarting the game
    public void showGameResultPopup(Board.Winner win) {
        if (win != Board.Winner.None) {
            gameOver = true;
            AlertDialog.Builder dialog = new AlertDialog.Builder(_context).setTitle("Konec hry!");
            if (win == Board.Winner.Draw) {
                dialog.setMessage("Remíza");
            } else if (win == Board.Winner.White) {
                dialog.setMessage("Bílý vyhrál");
            } else if (win == Board.Winner.Black) {
                dialog.setMessage("Černý vyhrál");
            }

            // Specifying a listener allows you to take an action before dismissing the dialog.
            // The dialog is automatically dismissed when a dialog button is clicked.
            dialog.setPositiveButton(R.string.action_restart, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    restart();
                }
            });

            // A null listener allows the button to dismiss the dialog and take no further action.
            dialog.setNegativeButton(android.R.string.ok, null);

            if (win == Board.Winner.White) {
                dialog.setIcon(R.drawable.white_king);
            } else if (win == Board.Winner.Black) {
                dialog.setIcon(R.drawable.black_king);
            } else if (win == Board.Winner.Draw) {
                dialog.setIcon(android.R.drawable.ic_menu_info_details);
            }
            dialog.show();
        }
    }

    public void undoMove() {
        if (historyIndex < 1) return; // no moves in history
        if (mode == 2 && !gameOver) return;
        if (mode == 1 && asyncAiTask != null) {
            asyncAiTask.cancel(true);
            waitingForAsyncAI = false;
        }
        historyIndex--;
        board = history.get(historyIndex).clone();
        updateUI();
    }

    public void redoMove() {
        if (historyIndex >= history.size() - 1) return; // no moves in history
        if (mode == 2 && !gameOver) return;
        historyIndex++;
        board = history.get(historyIndex).clone();
        updateUI();
    }


    public void init(TextView textScore, LinearLayout layoutMovesHistory, int mode, int saveMode, String save) {
        this.textScore = textScore;
        this.layoutMovesHistory = layoutMovesHistory;
        Log.d("CHESS", "Mode=" + mode);


        if (saveMode == -2) {
            loadFEN(save, mode);
        } else if (saveMode == -1 || saveMode == -3) {
            loadPGN(save, mode);
        } else {
            this.board = new Board(mode);
        }
        this.mode = mode;
    }

    public void loadFEN(String fenStr, int mode) {
        FEN fen = FEN.createBoardFromString(fenStr);

        if (fen.errored()) {
            Log.e("FEN", fen.getErrorMessage());
            Log.e("ChessBoard", "Invalid FEN:\n" + fenStr);
            Toast.makeText(_context, "Neplatný FEN", Toast.LENGTH_SHORT).show();
            if (!fenStr.equals(initialPosition)) {
                loadFEN(initialPosition, mode);
            }
            return;
        }

        board = new Board(mode);
        this.mode = mode;
        board.board = fen.getBoard();
        board.whitePlays = whiteStarts;

        Board.Winner win = Board.checkForWinner(board.board, board.whitePlays);
        board.winner = win;
        Log.d("DEBUG", "Winner=" + win.toString());
        showGameResultPopup(win);

        board.moveHistory.clear();
        history.clear();
        history.add(board.clone());

        updateUI();
        invalidate(); //redraw
    }

    public void loadPGN(String pgn, int mode) {
        history = PGN.loadFromPgn(pgn);
        if (history == null) {
            Log.e("ChessBoard", "Invalid PGN:" + pgn);
            Toast.makeText(_context, "Neplatný PGN", Toast.LENGTH_SHORT).show();
            loadFEN(initialPosition, mode);
            return;
        }
        historyIndex = history.size()-1;
        board = history.get(historyIndex);
        board.mode = mode;
        this.mode = mode;
        Board.Winner win = Board.checkForWinner(board.board, board.whitePlays);
        board.winner = win;
        Log.d("DEBUG", "Winner=" + win.toString());
        showGameResultPopup(win);

        updateUI();
        invalidate(); //redraw
    }

    public String saveFEN() {
        return board.saveFEN();
    }

    public String savePGN() {
        return PGN.saveToPgn(board);
    }

    public boolean changed() {
        return board.moveHistory.size() > 0;
    }
}
