package cz.ssps.chess.chess;

import cz.ssps.chess.pieces.Piece;

public abstract class ChessCallbacks {
    abstract void onPromotion(final boolean white, final int from, final int to, final Piece take);
}
