package cz.ssps.chess.chess;

import cz.ssps.chess.pieces.Bishop;
import cz.ssps.chess.pieces.King;
import cz.ssps.chess.pieces.Knight;
import cz.ssps.chess.pieces.Pawn;
import cz.ssps.chess.pieces.Piece;
import cz.ssps.chess.pieces.Queen;
import cz.ssps.chess.pieces.Rook;


public class FEN {
    public static final int WHITE_CASTLE_RIGHT = 0; // king side  = K
    public static final int WHITE_CASTLE_LEFT = 1; // queen side = Q
    public static final int BLACK_CASTLE_RIGHT = 2; // king side  = k
    public static final int BLACK_CASTLE_LEFT = 3; // queen side = q
    public static final char[] CASTLING_SYMBOLS = new char[]{'K', 'Q', 'k', 'q'};


    private Piece[] board;
    private boolean whitePlays;
    private boolean error;
    private String errorMessage;
    private boolean[] castling;
    private int[] enPassantTarget;
    private int halfMoves;
    private int fullMoves;

    public FEN(String errorMessage) {
        error = true;
        this.errorMessage = errorMessage;
    }

    public FEN(Piece[] board, boolean whitePlays, boolean[] castling, int[] enPassantTarget, int halfMoves, int fullMoves) {
        error = false;
        this.board = board;
        this.whitePlays = whitePlays;
        this.castling = castling;
        this.enPassantTarget = enPassantTarget;
        this.halfMoves = halfMoves;
        this.fullMoves = fullMoves;
    }

    public static FEN createBoardFromString(String fen) {
        Piece[] board = new Piece[Board.LEN];
        boolean whitePlays;
        boolean[] castling = new boolean[]{false, false, false, false}; // white right, white left, black right, black left
        int[] enPassantTarget = new int[]{-1, -1};
        int halfMoves = 0;
        int fullMoves = 0;

        try {

            String[] fields = fen.split(" ");

            if (fields.length != 6) {
                return new FEN("Invalid FEN. Expected 6 fields. Got " + fields.length);
            }

            // Piece placement
            String[] pieces = fields[0].split("/");
            for (int j = 0; j < Board.SIZE; j++) {
                int index = 0;
                for (int i = 0; i < Board.SIZE; ) {
                    if (index >= pieces[j].length()) {
                        return new FEN("Line " + (j + 1) + " in piece placement is too short");
                    }
                    char ch = pieces[j].charAt(index);

                    boolean white;

                    if (ch >= '0' && ch <= '9') {
                        i += Character.getNumericValue(ch); // skip squares - leave them null
                        index++;
                        continue;
                    } else if (ch >= 'A' && ch <= 'Z') {
                        white = true;
                        ch = (char) ((int) ch + 32); // convert to lowercase
                    } else if (ch >= 'a' && ch <= 'z') {
                        white = false;
                    } else {
                        return new FEN("Character " + ch + " at line " + (j + 1) + " at index " + index + " in piece placement is not alpha-numeric");
                    }
                    board[j * Board.SIZE + i] = Piece.fromSymbol(ch, white);
                    if ((!white || j != 6) && (white || j != 1)) {
                        board[j * Board.SIZE + i].moved();;
                    }
//                    switch (ch) {
//                        case 'p':
//                            if ((white && j == 6) || (!white && j == 1)) {
//                                board[j * Board.SIZE + i] = new Pawn(white, 0);
//                            } else {
//                                board[j * Board.SIZE + i] = new Pawn(white, 1);
//                            }
//                            break;
//                        case 'r':
//                            board[j * Board.SIZE + i] = new Rook(white, 1);
//                            break;
//                        case 'n':
//                            board[j * Board.SIZE + i] = new Knight(white);
//                            break;
//                        case 'b':
//                            board[j * Board.SIZE + i] = new Bishop(white);
//                            break;
//                        case 'q':
//                            board[j * Board.SIZE + i] = new Queen(white);
//                            break;
//                        case 'k':
//                            board[j * Board.SIZE + i] = new King(white, 1);
//                            break;
//                        default:
//                            return new FEN("Character " + ch + " at line " + (j + 1) + " at index " + index + " in piece placement doesn't match any of the piece symbols (p, r, n, b, q, k)");
//                    }
                    i++;
                    index++;
                }
            }

            // Active color
            if (fields[1].length() != 1) {
                return new FEN("Invalid active color field: " + fields[1]);
            }
            char color = fields[1].charAt(0);
            if (color == 'w') {
                whitePlays = true;
            } else if (color == 'b') {
                whitePlays = false;
            } else {
                return new FEN("Invalid character for active color: " + color + ". Must be either 'w' or 'b'");
            }


            // Castling rights
            if (fields[2].length() > 4) {
                return new FEN("Castling rights field too long. Expected 1-4, got " + fields[2].length());
            }
            for (char ch : fields[2].toCharArray()) {
                if (ch == 'K') {
                    castling[WHITE_CASTLE_RIGHT] = true;
                    board[Piece.RIGHT_WHITE_ROOK].undoMove(); // change moved status from 1 to 0
                    board[Piece.WHITE_KING].undoMove();
                } else if (ch == 'Q') {
                    castling[WHITE_CASTLE_LEFT] = true;
                    board[Piece.LEFT_WHITE_ROOK].undoMove();
                    board[Piece.WHITE_KING].undoMove();
                } else if (ch == 'k') {
                    castling[BLACK_CASTLE_RIGHT] = true;
                    board[Piece.RIGHT_BLACK_ROOK].undoMove();
                    board[Piece.BLACK_KING].undoMove();
                } else if (ch == 'q') {
                    castling[BLACK_CASTLE_LEFT] = true;
                    board[Piece.LEFT_BLACK_ROOK].undoMove();
                    board[Piece.BLACK_KING].undoMove();
                } else if (ch == '-') {
                    //leave all castling values at false
                } else {
                    return new FEN("Invalid character in castling rights field: " + ch);
                }
            }

            // Possible En Passant Targets
            if (!fields[3].equals("-")) {
                if (fields[3].length() != 2) {
                    return new FEN("Invalid position for en passant. Expected two chars (eg. a5). Got " + fields[3]);
                }
                char a = fields[3].charAt(0);
                char b = fields[3].charAt(1);
                int i = (int) a - (int) 'a';
                int j = 7 - ((int) b - (int) '1');
                if (j < 0 || j > Board.SIZE) {
                    return new FEN("Invalid position for en passant. First coordinate must be within 'a'-'h' Got " + a);
                }
                if (i < 0 || i > Board.SIZE) {
                    return new FEN("Invalid position for en passant. Second coordinate must be within '1'-'8' Got " + b);
                }

                // move to the square the pawn is on
                if (j == 2) {
                    j = 3;
                } else if (j == 5) {
                    j = 4;
                }
                enPassantTarget[0] = j;
                enPassantTarget[1] = i;

                Piece p = board[j * Board.SIZE + i];
                if (p == null) {
                    return new FEN("Square at position " + fields[3] + " is not a valid en passant target");
                }
                if (p instanceof Pawn) {
                    Pawn pawn = (Pawn) p;
                    pawn.setEnPassantFlag(); // set en passant flag
                } else {
                    return new FEN("Piece at position " + fields[3] + " is not a pawn");
                }
            }

            // Half moves
            try {
                halfMoves = Integer.parseInt(fields[4]);
            } catch (NumberFormatException e) {
                return new FEN("Invalid halfMoves field. Expected number. Got " + fields[4]);
            }


            // Full moves
            try {
                fullMoves = Integer.parseInt(fields[5]);
            } catch (NumberFormatException e) {
                return new FEN("Invalid fullMoves field. Expected number. Got " + fields[5]);
            }

        } catch (Exception e) {
            return new FEN("Invalid FEN (" + e.toString() + ")");
        }
        return new FEN(board, whitePlays, castling, enPassantTarget, halfMoves, fullMoves);
    }

    public static String boardToFEN(Piece[] board, boolean whitePlays) {
        StringBuilder fen = new StringBuilder();

        // Piece placement
        for (int j = 0; j < Board.SIZE; j++) {
            int empty = 0; // number of empty squares
            for (int i = 0; i < Board.SIZE; i++) {
                Piece p = board[j * Board.SIZE + i];
                if (p != null) {
                    if (empty > 0) {
                        fen.append(empty);
                        empty = 0;
                    }
                    fen.append(p.getSymbol());
                } else {
                    empty++;
                }
            }
            if (empty > 0) {
                fen.append(empty);
            }
            if (j < Board.SIZE - 1) {
                fen.append('/');
            }
        }


        fen.append(' '); // field separator


        // Active color
        fen.append(whitePlays ? 'w' : 'b');


        fen.append(' '); // field separator


        // Castling rights
        boolean[] castling = new boolean[]{true, true, true, true};
        // white
        int wki = Board.whiteKingIndex(board);
        if (board[wki].hasMoved()) {
            castling[WHITE_CASTLE_LEFT] = false;
            castling[WHITE_CASTLE_RIGHT] = false;
        } else {
            Piece leftRook = board[Piece.LEFT_WHITE_ROOK];
            if (leftRook == null) {
                castling[WHITE_CASTLE_LEFT] = false;
            } else if (leftRook.isWhite() ^ whitePlays || !(leftRook instanceof Rook) || leftRook.hasMoved()) {
                castling[WHITE_CASTLE_LEFT] = false;
            }
            Piece rightRook = board[Piece.LEFT_WHITE_ROOK];
            if (rightRook == null) {
                castling[WHITE_CASTLE_RIGHT] = false;
            } else if (rightRook.isWhite() ^ whitePlays || !(rightRook instanceof Rook) || rightRook.hasMoved()) {
                castling[WHITE_CASTLE_RIGHT] = false;
            }
        }
        // black
        int bki = Board.blackKingIndex(board);
        if (board[bki].hasMoved()) {
            castling[BLACK_CASTLE_LEFT] = false;
            castling[BLACK_CASTLE_RIGHT] = false;
        } else {
            Piece leftRook = board[Piece.LEFT_BLACK_ROOK];
            if (leftRook == null) {
                castling[BLACK_CASTLE_LEFT] = false;
            } else if (leftRook.isWhite() ^ !whitePlays || !(leftRook instanceof Rook) || leftRook.hasMoved()) {
                castling[BLACK_CASTLE_LEFT] = false;
            }
            Piece rightRook = board[Piece.LEFT_BLACK_ROOK];
            if (rightRook == null) {
                castling[BLACK_CASTLE_RIGHT] = false;
            } else if (rightRook.isWhite() ^ !whitePlays || !(rightRook instanceof Rook) || rightRook.hasMoved()) {
                castling[BLACK_CASTLE_RIGHT] = false;
            }
        }

        boolean none = true;
        for (int i = 0; i < castling.length; i++) {
            if (castling[i]) {
                fen.append(CASTLING_SYMBOLS[i]);
                none = false;
            }
        }
        if (none) {
            fen.append('-');
        }


        fen.append(' '); // field separator


        // Possible En Passant Targets
        boolean hasEnPassant = false;
        for (int n = 0; n < Board.LEN; n++) {
            Piece p = board[n];
            if (p != null) {
                if (p instanceof Pawn) {
                    if (((Pawn) p).hasEnPassantFlag()) {
                        int i = n % Board.SIZE;
                        int j = n / Board.SIZE;
                        if (j == 4) {
                            j = 5;
                        } else if (j == 3) {
                            j = 2;
                        }

                        char a = (char) ((int) 'a' + i);
                        char b = (char) ((int) '1' + j);
                        fen.append(a);
                        fen.append(b);
                        hasEnPassant = true;
                        break;
                    }
                }
            }
        }
        if (!hasEnPassant) {
            fen.append('-');
        }


        fen.append(' '); // field separator


        // Half moves
        fen.append('0');


        fen.append(' '); // field separator


        // Full moves
        fen.append('1');

        return fen.toString();
    }

    public boolean errored() {
        return error;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public Piece[] getBoard() {
        return board;
    }

    public boolean whitePlays() {
        return whitePlays;
    }

    public boolean canWhiteCastleRight() {
        return castling[WHITE_CASTLE_RIGHT];
    }

    public boolean canWhiteCastleLeft() {
        return castling[WHITE_CASTLE_LEFT];
    }

    public boolean canBlackCastleRight() {
        return castling[BLACK_CASTLE_RIGHT];
    }

    public boolean canBlackCastleLeft() {
        return castling[BLACK_CASTLE_LEFT];
    }

    public boolean hasEnPassantTarget() {
        return enPassantTarget[0] != -1 && enPassantTarget[1] != -1;
    }

    public int[] getEnPassantTarget() {
        return enPassantTarget;
    }

    public int getHalfMoves() {
        return halfMoves;
    }

    public int getFullMoves() {
        return fullMoves;
    }
}
