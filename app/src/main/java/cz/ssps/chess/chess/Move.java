package cz.ssps.chess.chess;

import androidx.annotation.NonNull;

import cz.ssps.chess.pieces.Pawn;
import cz.ssps.chess.pieces.Piece;

public class Move {
    public int from;
    public int to;
    public Piece piece;
    public Piece capture; // can be null if no piece was taken
    public Piece promotion; // can be null, is set when a pawn was promoted

    public Move() {
        this.from = -1;
        this.to = -1;
    }

    public Move(int from, int to, Piece piece, Piece take, Piece promotion) {
        this.from = from;
        this.to = to;
        this.piece = piece;
        this.capture = take;
        this.promotion = promotion;
    }

    public Move(String from, String to, Piece piece, Piece take, Piece promotion) {
        this.from = fromString(from);
        this.to = fromString(to);
        this.piece = piece;
        this.capture = take;
        this.promotion = promotion;
    }

    public static int fromString(String square) {
        char a = square.charAt(0);
        char b = square.charAt(1);
        return fromChars(a, b);
    }

    public static int fromChars(char a, char b) {
        int i = (int) a - (int) 'a';
        int j = 7 - ((int) b - (int) '1');
        return j * Board.SIZE + i;
    }

    @NonNull
    @Override
    public String toString() {
        char xFrom = (char) ((int) 'a' + (from % 8));
        char yFrom = (char) ((int) '1' + (7 - (from / 8)));
        char xTo = (char) ((int) 'a' + (to % 8));
        char yTo = (char) ((int) '1' + (7 - (to / 8)));
        return "" + xFrom + yFrom + "->" + xTo + yTo + (capture != null ? (" x" + capture.getSymbol()) : "") + (promotion != null ? (" ^" + promotion.getSymbol()) : "");
    }

    @NonNull
    public String toNotation(int index) {
        StringBuilder notation = new StringBuilder();
        notation.append(index);
        notation.append(". ");
        if (!(piece instanceof Pawn)) {
            char p = Character.toUpperCase(piece.getSymbol());
            notation.append(p);
        }
        if (capture != null) {
            notation.append("x");
        }
        char xFrom = (char) ((int) 'a' + (from % 8));
        char yFrom = (char) ((int) '1' + (7 - (from / 8)));
        char xTo = (char) ((int) 'a' + (to % 8));
        char yTo = (char) ((int) '1' + (7 - (to / 8)));

        notation.append(xFrom);
        notation.append(yFrom);

        notation.append(xTo);
        notation.append(yTo);

        //TODO: castling
        //TODO: check: append '+'
        //TODO: mate: append '#'

        if (promotion != null) {
            notation.append('=');
            notation.append(Character.toUpperCase(promotion.getSymbol()));
        }

        return notation.toString();
    }
}
