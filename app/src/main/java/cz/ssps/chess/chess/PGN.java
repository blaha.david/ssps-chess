package cz.ssps.chess.chess;

import static cz.ssps.chess.chess.ChessBoardView.initialPosition;

import android.icu.text.SimpleDateFormat;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import cz.ssps.chess.app.MainActivity;
import cz.ssps.chess.pieces.Piece;

public class PGN {
    public static String saveToPgn(Board board) {
        StringBuilder pgn = new StringBuilder();


        // Seven Tag Roster
        pgn.append("[Event \"Chess\"]\n");
        pgn.append("[Site \"Chess app\"]\n");

        pgn.append("[Date \"");
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy.MM.dd");
        Date today = Calendar.getInstance().getTime();
        pgn.append(formatter.format(today));
        pgn.append("\"]\n");

        pgn.append("[EventDate \"?\"]\n");
        pgn.append("[Round \"?\"]\n");


        switch (board.winner) {
            case None:
                pgn.append("[Result \"*\"]\n");
                break;
            case White:
                pgn.append("[Result \"1-0\"]\n");
                break;
            case Black:
                pgn.append("[Result \"0-1\"]\n");
                break;
            case Draw:
                pgn.append("[Result \"1/2-1/2\"]\n");
                break;
        }

        switch (board.mode) {
            case 0:
                pgn.append("[White \"Player1\"]\n");
                pgn.append("[Black \"Player2\"]\n");
                break;
            case 1:
                pgn.append("[White \"Player1\"]\n");
                pgn.append("[Black \"AI\"]\n");
                break;
            case 2:
                pgn.append("[White \"AI\"]\n");
                pgn.append("[Black \"AI\"]\n");
                break;
        }

        pgn.append("\n");

        int moveNumber = 1;
        for (Move m : board.moveHistory) {
            pgn.append(m.toNotation(moveNumber));
            pgn.append(" ");
            moveNumber++;
        }

        return pgn.toString().trim();
    }

    public static ArrayList<Board> loadFromPgn(String pgn) {
        ArrayList<Board> history = new ArrayList<>();

        try {

            String[] sections = pgn.split("\n\n");

            String[] sevenTagRoster = sections[0].split("\n");

            String[] moves = sections[1].split("\\d+\\. ");

            Board b = new Board(0); //TODO: extract mode info

            FEN fen = FEN.createBoardFromString(initialPosition);
            b.board = fen.getBoard();
            b.whitePlays = true;

            history.add(b.clone());

            for (int i = 1; i < moves.length; i++) {
                String moveStr = moves[i];
                Log.d("PGN", moveStr);

                int offset = 0;
                char first = moveStr.charAt(offset);
                char promotedPiece = '\0';
                if (Character.isUpperCase(first)) {
                    offset++;
                }
                if (moveStr.charAt(offset) == 'x') {
                    // Capture
                    offset++;
                }
                char xFrom = moveStr.charAt(offset++);
                char yFrom = moveStr.charAt(offset++);
                char xTo = moveStr.charAt(offset++);
                char yTo = moveStr.charAt(offset++);

                int from = Move.fromChars(xFrom, yFrom);
                int to = Move.fromChars(xTo, yTo);

                if (offset < moveStr.length() - 1) {
                    // Promotion
                    offset++; // Skip '='
                    promotedPiece = moveStr.charAt(offset);
                }

                b.move(new Move(from, to, b.getPiece(from), b.getPiece(to), Piece.fromSymbol(promotedPiece, b.whitePlays)), false, null);

                history.add(b.clone());
            }

            return history;
        } catch (Exception e) {
            return null;
        }
    }

}
