package cz.ssps.chess.pieces;


import cz.ssps.chess.chess.Board;

public class Bishop extends Piece {
    public static final int IMAGE_INDEX = 2;

    private final int[] validMovesRightDown = new int[] {9, 18, 27, 36, 45, 54, 63};
    private final int[] validMovesLeftUp = new int[] {-63, -54, -45, -36, -27, -18, -9};
    private final int[] validMovesLeftDown = new int[] {49, 42, 35, 28, 21, 14, 7};
    private final int[] validMovesRightUp = new int[] {-7, -14, -21, -28, -35, -42, -49};

    public Bishop (boolean white) {
        super(white, IMAGE_INDEX);
    }

    @Override
    public boolean validMove (int from, int to, Piece[] board) {
        if (to < 0 || to >= Board.LEN) {
            return false;
        }
        if (board[to] != null) {
            if (board[to].isWhite() == isWhite()) {
                return false;
            }
        }
        boolean blocked = false;
        for (int i = 0; i < validMovesRightDown.length - (from % 8); i++) {
            int index = validMovesRightDown[i] + from;
            if (index < 0 || index >= Board.LEN) continue;
            if (board[index] != null) {
                if (index == to && !blocked) {
                    return true;
                }
                blocked = true;
            }
            if (index == to && !blocked) {
                return true;
            }
        }
        blocked = false;
        for (int i = validMovesLeftUp.length-1; i >= (7 - (from % 8)); i--) {
            int index = validMovesLeftUp[i] + from;
            if (index < 0 || index >= Board.LEN) continue;
            if (board[index] != null) {
                if (index == to && !blocked) {
                    return true;
                }
                blocked = true;
            }
            if (index == to && !blocked) {
                return true;
            }
        }
        blocked = false;
        for (int i = validMovesLeftDown.length-1; i >= (7 - (from % 8)); i--) {
            int index = validMovesLeftDown[i] + from;
            if (index < 0 || index >= Board.LEN) continue;
            if (board[index] != null) {
                if (index == to && !blocked) {
                    return true;
                }
                blocked = true;
            }
            if (index == to && !blocked) {
                return true;
            }
        }
        blocked = false;
        for (int i = 0; i < validMovesRightUp.length - (from % 8); i++) {
            int index = validMovesRightUp[i] + from;
            if (index < 0 || index >= Board.LEN) continue;
            if (board[index] != null) {
                if (index == to && !blocked) {
                    return true;
                }
                blocked = true;
            }
            if (index == to && !blocked) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int getPieceValue() {
        return 3;
    }

    @Override
    public Piece copy() {
        return new Bishop(isWhite());
    }

    @Override
    public char getSymbol() { return isWhite() ? 'B': 'b'; }
}
