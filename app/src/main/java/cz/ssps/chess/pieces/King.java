package cz.ssps.chess.pieces;


import cz.ssps.chess.chess.Board;

public class King extends Piece {
    public static final int IMAGE_INDEX = 0;

    //indexes on the board
    public static final int LEFT_WHITE_CASTLE = 58;
    public static final int RIGHT_WHITE_CASTLE = 62;
    public static final int LEFT_BLACK_CASTLE = 2;
    public static final int RIGHT_BLACK_CASTLE = 6;


    private final int[] validMovesRight = new int[] {-7, 1, 9};
    private final int[] validMovesLeft = new int[] {-9, -1, 7};
    private final int[] validMovesDown = new int[] {8};
    private final int[] validMovesUp = new int[] {-8};

    public King (boolean white) {
        super(white, IMAGE_INDEX);
    }

    public King(boolean white, int moved) { // used for deep copy
        super(white, IMAGE_INDEX);
        this.moved = moved;
    }

    @Override
    public boolean validMove (int from, int to, Piece[] board) {
        if (to < 0 || to >= Board.LEN) {
            return false;
        }
        if (board[to] != null) {
            if (board[to].isWhite() == isWhite()) {
                return false;
            }
        }
        for (int i = 0; i < validMovesRight.length; i++) {
            if (validMovesRight[i] + from == to && !(from % 8 == 7)) {
                return true;
            }
        }
        for (int i = 0; i < validMovesLeft.length; i++) {
            if (validMovesLeft[i] + from == to && !(from % 8 == 0)) {
                return true;
            }
        }
        for (int i = 0; i < validMovesDown.length; i++) {
            if (validMovesDown[i] + from == to) {
                return true;
            }
        }
        for (int i = 0; i < validMovesUp.length; i++) {
            if (validMovesUp[i] + from == to) {
                return true;
            }
        }

        // CASTLING
        if (isWhite()) {
            if (to == LEFT_WHITE_CASTLE) {// left castle
                if (hasMoved()) { // cannot castle if the king has moved
                    return false;
                }
                Piece leftRook = board[LEFT_WHITE_ROOK];
                if (leftRook != null) { // if the left rook is still there
                    if (leftRook instanceof Rook && leftRook.isWhite() && !leftRook.hasMoved()) { // if it really is a white rook and not any other piece and it didn't move yet
                        // check if there free space - no pieces are blocking them
                        for (int i = LEFT_WHITE_ROOK + 1; i < from; i++) { //check all tiles between the rook and the king
                            if (board[i] != null) {
                                return false; // a piece is blocking it - cannot castle
                            }
                        }

                        return true; // can castle - no pieces are in the way
                    }
                }
            }
            if (to == RIGHT_WHITE_CASTLE) {// right castle
                if (hasMoved()) { // cannot castle if the king has moved
                    return false;
                }
                Piece rightRook = board[RIGHT_WHITE_ROOK];
                if (rightRook != null) { // if the left rook is still there
                    if (rightRook instanceof Rook && rightRook.isWhite() && !rightRook.hasMoved()) { // if it really is a white rook and not any other piece and it didn't move yet
                        // check if there free space - no pieces are blocking them
                        for (int i = from + 1; i < RIGHT_WHITE_ROOK; i++) { //check all tiles between the rook and the king
                            if (board[i] != null) {
                                return false; // a piece is blocking it - cannot castle
                            }
                        }

                        return true; // can castle - no pieces are in the way
                    }
                }
            }
        } else { // black castle:

            if (to == LEFT_BLACK_CASTLE) {// left castle
                if (hasMoved()) { // cannot castle if the king has moved
                    return false;
                }
                Piece leftRook = board[LEFT_BLACK_ROOK];
                if (leftRook != null) { // if the left rook is still there
                    if (leftRook instanceof Rook && !leftRook.isWhite() && !leftRook.hasMoved()) { // if it really is a black rook and not any other piece and it didn't move yet
                        // check if there free space - no pieces are blocking them
                        for (int i = LEFT_BLACK_ROOK + 1; i < from; i++) { //check all tiles between the rook and the king
                            if (board[i] != null) {
                                return false; // a piece is blocking it - cannot castle
                            }
                        }

                        return true; // can castle - no pieces are in the way
                    }
                }
            }
            if (to == RIGHT_BLACK_CASTLE) {// right castle
                if (hasMoved()) { // cannot castle if the king has moved
                    return false;
                }
                Piece rightRook = board[RIGHT_BLACK_ROOK];
                if (rightRook != null) { // if the left rook is still there
                    if (rightRook instanceof Rook && !rightRook.isWhite() && !rightRook.hasMoved()) { // if it really is a black rook and not any other piece and it didn't move yet
                        // check if there free space - no pieces are blocking them
                        for (int i = from + 1; i < RIGHT_BLACK_ROOK; i++) { //check all tiles between the rook and the king
                            if (board[i] != null) {
                                return false; // a piece is blocking it - cannot castle
                            }
                        }

                        return true; // can castle - no pieces are in the way
                    }
                }
            }
        }

        return false;
    }

    @Override
    public int getPieceValue() {
        return 999;
    }

    @Override
    public Piece copy() {
        return new King(isWhite(), moved);
    }

    @Override
    public char getSymbol() { return isWhite() ? 'K': 'k'; }
}
