package cz.ssps.chess.pieces;


import cz.ssps.chess.chess.Board;

public class Knight extends Piece {
    public static final int IMAGE_INDEX = 3;

    private final int[] validMovesRight = new int[] {-6, 10};
    private final int[] validMovesLeft = new int[] {-10, 6};
    private final int[] validMovesDown = new int[] {15, 17};
    private final int[] validMovesUp = new int[] {-17, -15};

    public Knight (boolean white) {
        super(white, IMAGE_INDEX);
    }

    @Override
    public boolean validMove (int from, int to, Piece[] board) {
        if (to < 0 || to >= Board.LEN) {
            return false;
        }
        if (board[to] != null) {
            if (board[to].isWhite() == isWhite()) {
                return false;
            }
        }
        if ((validMovesRight[0] + from == to || validMovesRight[1] + from == to) && from % 8 != 7 && from % 8 != 6) {
            return true;
        }
        if ((validMovesLeft[0] + from == to || validMovesLeft[1] + from == to) && from % 8 != 0 && from % 8 != 1) {
            return true;
        }
        if ((validMovesDown[0] + from == to && from % 8 != 0) || (validMovesDown[1] + from == to && from % 8 != 7)) {
            return true;
        }
        if ((validMovesUp[0] + from == to && from % 8 != 0) || (validMovesUp[1] + from == to && from % 8 != 7)) {
            return true;
        }
        return false;
    }

    @Override
    public int getPieceValue() {
        return 3;
    }

    @Override
    public Piece copy() {
        return new Knight(isWhite());
    }

    @Override
    public char getSymbol() { return isWhite() ? 'N': 'n'; }
}
