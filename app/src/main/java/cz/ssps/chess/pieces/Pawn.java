package cz.ssps.chess.pieces;


import android.util.Log;

public class Pawn extends Piece {
    public static final int IMAGE_INDEX = 5;

    private boolean enPassant = false; //set on the pawn, that moved two tiles and can be taken

    public Pawn (boolean white) {
        super(white, IMAGE_INDEX);
    }
    public Pawn (boolean white, int moved) { // used for deep copy
        super(white, IMAGE_INDEX);
        this.moved = moved;
    }

    @Override
    public boolean validMove (int from, int to, Piece[] board) {

        // cross take
        int i = from + (isWhite() ? -9 : 7); // to the left
        if (i == to && from % 8 != 0) {
            Piece p = board[i];
            if (p != null) {
                if (p.isWhite() ^ isWhite()) {
                    return true;
                }
            }
        }
        i = from + (isWhite() ? -7 : 9); // to the right
        if (i == to && from % 8 != 7) {
            Piece p = board[i];
            if (p != null) {
                if (p.isWhite() ^ isWhite()) {
                    return true;
                }
            }
        }

        i = from + (isWhite() ? -8 : 8);
        if (i == to) {
            Piece p = board[i];
            if (p == null) { // can move forward only if there is an empty tile - cannot take forward
                return true;
            }
        }

        if (canMoveTwice(from, to, board)) {
            return true;
        }

        //en passant
        if (canEnPassant(from, to, board)) {
            return true;
        }

        return false;
    }
    public boolean canMoveTwice (int from, int to, Piece[] board) {
        // if it didn't move yet, it can go two tiles forward
        if (!hasMoved()) {
            int i = from + (isWhite() ? -16 : 16);
            if (i == to) {
                Piece p = board[i];
                if (p == null && board[from + (isWhite() ? -8 : 8)] == null) { // can move forward only if there is an empty tile - cannot take forward
                    return true;
                }
            }
        }
        return false;
    }

    public boolean canEnPassant(int from, int to, Piece[] board) {
        int i = from + (isWhite() ? -9 : 7); // to the left
        if (i == to && from % 8 != 0) {
            if (board[to] == null) {  // the diagonal tile is empty
                Piece p = board[from-1];// and the piece next to the pawn is the opposite color
                if (p != null){
                    if (p.isWhite() ^ isWhite() && p instanceof Pawn) {
                        if (((Pawn)p).hasEnPassantFlag()) {
                            return true;
                        }
                    }
                }
            }
        }
        i = from + (isWhite() ? -7 : 9); // to the right
        if (i == to && from % 8 != 7) {
            if (board[to] == null) {
                Piece p = board[from+1];
                if (p != null){
                    if (p.isWhite() ^ isWhite() && p instanceof Pawn) {
                        if (((Pawn)p).hasEnPassantFlag()) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    public void setEnPassantFlag () {
        enPassant = true;
    }
    public void removeEnPassantFlag () {
        enPassant = false;
    }

    public boolean hasEnPassantFlag () {
        return enPassant;
    }

    public boolean isTileAttacked (int from, int to, Piece[] board) {
        // cross take
        int i = from + (isWhite() ? -9 : 7); // to the left
        if (i == to && from % 8 != 0) {
            return true;
        }
        i = from + (isWhite() ? -7 : 9); // to the right
        if (i == to && from % 8 != 7) {
            return true;
        }
        return false;
    }

    @Override
    public int getPieceValue() {
        return 1;
    }

    @Override
    public Piece copy() {
        Pawn p = new Pawn(isWhite(), moved);
        if (this.hasEnPassantFlag()) {
            p.setEnPassantFlag();
        }
        return p;
    }

    @Override
    public char getSymbol() { return isWhite() ? 'P': 'p'; }
}
