package cz.ssps.chess.pieces;

import androidx.annotation.NonNull;

import cz.ssps.chess.chess.Board;
import cz.ssps.chess.chess.FEN;


// Source: https://commons.wikimedia.org/wiki/File:Chess_Pieces_Sprite.svg

public abstract class Piece {
    // indexes on default board
    public static final int LEFT_WHITE_ROOK = 56;
    public static final int RIGHT_WHITE_ROOK = 63;
    public static final int LEFT_BLACK_ROOK = 0;
    public static final int RIGHT_BLACK_ROOK = 7;
    public static final int WHITE_KING = 60;
    public static final int BLACK_KING = 4;

    private boolean white;
    private int imageIndex;

    protected int moved = 0;

    public Piece (boolean white, int imageIndex) {
        this.white = white;
        this.imageIndex = imageIndex + (white ? 0 : 6);
    }

    public boolean isWhite () {
        return white;
    }

    public int getImageIndex () {
        return imageIndex;
    }

    public void moved() {
        moved++;
    }
    public void undoMove () {
        if (hasMoved()) {
            moved--;
        }
    }
    public boolean hasMoved () {
        return moved > 0;
    }

    @NonNull
    @Override
    public String toString() {
        return ""+getSymbol();
    }

    public abstract boolean validMove (int from, int to, Piece[] board);
    public abstract int getPieceValue ();
    public abstract char getSymbol ();
    public abstract Piece copy();

    public static Piece fromSymbol (char symbol, boolean white) {
        char ch = Character.toUpperCase(symbol);
        switch (ch) {
            case 'P':
                return new Pawn(white, 0);
            case 'R':
                return new Rook(white, 0);
            case 'N':
                return new Knight(white);
            case 'B':
                return new Bishop(white);
            case 'Q':
                return new Queen(white);
            case 'K':
                return new King(white, 0);
            default:
                return null;
        }
    }
}
