package cz.ssps.chess.pieces;

import cz.ssps.chess.chess.Board;

public class Rook extends Piece {
    public static final int IMAGE_INDEX = 4;

    private final int[] validMovesRight = new int[] {1, 2, 3, 4, 5, 6, 7};
    private final int[] validMovesLeft = new int[] {-1, -2, -3, -4, -5, -6, -7};
    private final int[] validMovesDown = new int[] {8, 16, 24, 32, 40, 48, 56};
    private final int[] validMovesUp = new int[] {-8, -16, -24, -32, -40, -48, -56};

    public Rook (boolean white) {
        super(white, IMAGE_INDEX);
    }
    public Rook(boolean white, int moved) { // used for deep copy
        super(white, IMAGE_INDEX);
        this.moved = moved;
    }

    @Override
    public boolean validMove (int from, int to, Piece[] board) {
        if (to < 0 || to >= Board.LEN) {
            return false;
        }
        if (board[to] != null) {
            if (board[to].isWhite() == isWhite()) {
                return false;
            }
        }
        boolean blocked = false;
        for (int i = 0; i < validMovesRight.length - (from % 8); i++) {
            int index = validMovesRight[i] + from;
            if (index < 0 || index >= Board.LEN) continue;
            if (board[index] != null) {
                if (index == to && !blocked) {
                    return true;
                }
                blocked = true;
            }
            if (index == to && !blocked) {
                return true;
            }
        }

        blocked = false;
        for (int i = 0; i < validMovesLeft.length - (7 - (from % 8)); i++) {
            int index = validMovesLeft[i] + from;
            if (index < 0 || index >= Board.LEN) continue;
            if (board[index] != null) {
                if (index == to && !blocked) {
                    return true;
                }
                blocked = true;
            }
            if (index == to && !blocked) {
                return true;
            }
        }

        blocked = false;
        for (int i = 0; i < validMovesDown.length; i++) {
            int index = validMovesDown[i] + from;
            if (index < 0 || index >= Board.LEN) continue;
            if (board[index] != null) {
                if (index == to && !blocked) {
                    return true;
                }
                blocked = true;
            }
            if (index == to && !blocked) {
                return true;
            }
        }

        blocked = false;
        for (int i = 0; i < validMovesUp.length; i++) {
            int index = validMovesUp[i] + from;
            if (index < 0 || index >= Board.LEN) continue;
            if (board[index] != null) {
                if (index == to && !blocked) {
                    return true;
                }
                blocked = true;
            }
            if (index == to && !blocked) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int getPieceValue() {
        return 5;
    }

    @Override
    public Piece copy() {
        return new Rook(isWhite(), moved);
    }

    @Override
    public char getSymbol() { return isWhite() ? 'R': 'r'; }
}
